// server.js
const jsonServer = require("json-server");
const moment = require("moment");
const server = jsonServer.create();
const middlewares = jsonServer.defaults();
const path = require("path");
const router = jsonServer.router(path.join(__dirname, "db.json"));
var database = require('./db.json');
var fs = require('fs');
var current_id = ''

const userObj = {
  id: 128,
  username: "John Smith",
  avatar: "http://i.pravatar.cc/300?u=JohnSmith",
  email: "js@example.com",
  likes_count: 12,
  goings_count: 2,
  past_count: 3
};

// "database"
let likedEvents = {};
let goingEvents = {};
let eventToChannel = {};

let eventsList = [];
let eventObjects = {};

const setup = () => {
  for (let index = 0; index < 1000; index++) {
    let currentEvent = eventTemplate(index, getRandomChannel(), false);
    eventsList.push(currentEvent);
    eventObjects[currentEvent.id] = currentEvent;
  }
};

const likedEventsList = () => {
  let likedKeys = Object.keys(likedEvents)
    .filter(k => likedEvents[k])
    .map(s => parseInt(s, 10));

  let likedList = [];
  likedKeys.map(key => {
    if (eventObjects[key] && eventObjects[key].is_liked) {
      likedList.push(eventObjects[key]);
    }
  });
  return likedList;
};

const goingEventsList = () => {
  let goingKeys = Object.keys(goingEvents)
    .filter(k => goingEvents[k])
    .map(s => parseInt(s, 10));
  let goingList = [];
  goingKeys.map(key => {
    if (eventObjects[key] && eventObjects[key].is_going) {
      goingList.push(eventObjects[key]);
    }
  });
  return goingList;
};

server.use(middlewares);
server.use(jsonServer.bodyParser);
server.post("/auth/token", (req, res) => { //Login authentication
  database['users'].forEach(user => {
    if (req.body.email === user['email'] && req.body.password === user['password']) {
        res.json({
          id: user['id']
        });
    }
  })
  if (res.json().length < 1) res.sendStatus(403);
});

server.post("/users", (req, res) => { //Register new user
  if (database['users'].every(user => {
    if (req.body.email != user['email']) {
        return true;
    } else return false;
  })) {
    let new_user = {
      "name" : req.body.name,
      "email" : req.body.email,
      "password" : req.body.password,
      "id" : database["users"][database["users"].length - 1]["id"] + 1
    }
    database["users"].push(new_user)
    fs.writeFile('api/db.json', JSON.stringify(database, null, "\t"),function(err){
      if(err) throw err;
    })
    res.json({
      id: new_user['id']
    });
  }
  else res.json("failed")
});

server.get("/users/:id", (req, res) => { //Get user info
  let get_user = {}
  database['users'].forEach(user => {
    if (user['id'] == parseInt(req.params.id)) {
      get_user = {
        name : user['name'],
        email : user['email']
      }
    }
  })
  res.json(get_user)
});

server.post("/event", (req, res) => { //Get user info
  let creator = {}
  database['users'].forEach(user => {
    if (user['id'] == req.body.id) {
      creator = {
        name : user['name'],
        email : user['email']
      }
    }
  })

  let new_event = {
    "name" : creator.name,
    "email" : creator.email,
    "title" : req.body.title,
    "content" : req.body.content,
    "likes" : 0,
    "create_time": moment(),
    "comments": []
  }
  database["events"].push(new_event)
  fs.writeFile('api/db.json', JSON.stringify(database, null, "\t"),function(err){
    if(err) throw err;
  })
  res.json(new_event);
});

const channelChoices = [
  {
    id: 5,
    name: "Channel One"
  },
  {
    id: 6,
    name: "Park"
  },
  {
    id: 7,
    name: "Parades"
  },
  {
    id: 8,
    name: "Drinks"
  },
  {
    id: 9,
    name: "Food"
  }
];

const getRandomChannel = () =>
  channelChoices[getRandomInt(0, channelChoices.length)];

const defaultChannel = {
  id: 5,
  name: "Channel One"
};

const MAX_EVENTS = 227;

// Add new endpoint to aid the sidemenu
server.get("/channels", (req, res) => {
  res.json({
    channels: channelChoices
  });
});

server.get("/events", (req, res) => {
  const offset = parseInt(req.query.offset || "0", 10);
  let channel = null;
  if (req.query.channel) {
    channel = channels[req.query.channel];
  }
  let start = null;
  let end = null;
  if (req.query.after) {
    // Convert from miliseconds
    start = parseInt(req.query.after, 10);
    end = parseInt(req.query.before, 10);
    // res.json({ events: [], has_more: false });
    // return;
  }
  let filteredByChannel = eventsList.filter(event => {
    return channel ? event.channel.id === channel.id : true;
  });
  console.log(`Filtered by channel: ${filteredByChannel.length}`);
  let filteredByTime = filteredByChannel.filter(event => {
    if (start && end) {
      return event.begin_time >= start && event.end_time <= end;
    }
    return true;
  });
  console.log(`Filtered by time: ${filteredByTime.length}`);
  let arr = [];
  for (var i = offset; i < Math.min(offset + 10, MAX_EVENTS); i++) {
    arr.push(filteredByTime[i]);
  }
  arr = filteredByTime.slice(
    offset,
    Math.min(offset + 10, filteredByTime.length)
  );
  res.json({
    events: arr,
    has_more: !(offset + 10 >= filteredByTime.length)
  });
});

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

const userGen = (req, res) => {
  const n = getRandomInt(0, 20);
  let arr = [];
  for (var i = 0; i < n; i++) {
    arr.push({
      id: getRandomInt(5000, 50000),
      username: "Commenter " + getRandomInt(5000, 50000),
      avatar: `http://i.pravatar.cc/300?u=${getRandomInt(5000, 50000)}`
    });
  }

  res.json(arr);
};

server.get("/event/:id/participants", userGen);
server.get("/event/:id/likes", userGen);

server.get("/event/:id/comments", (req, res) => {
  const offset = parseInt(req.query.offset || "0", 10);
  const n = getRandomInt(10000, 100000);
  let arr = [];
  for (var i = offset; i < Math.min(offset + 20, 77); i++) {
    arr.push({
      id: n + i,
      create_time: 1536461247 - i * 600,
      comment:
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium nemo culpa facere quaerat quas labore deserunt porro possimus doloremque nihil!",
      author: {
        id: getRandomInt(5000, 50000),
        username: "Commenter " + getRandomInt(5000, 50000),
        avatar: `http://i.pravatar.cc/300?u=${getRandomInt(5000, 50000)}`
      }
    });
  }
  res.json({
    comments: arr,
    has_more: !(offset + 20 >= 77)
  });
});

server.post("/event/:id/comments", (req, res) => {
  const comment = {
    id: getRandomInt(0, 2000000),
    create_time: moment().unix(),
    comment: req.body.comment,
    author: {
      id: userObj.id,
      username: userObj.username,
      avatar: userObj.avatar
    }
  };

  // Find current event
  let activityIndex = eventsList.findIndex(
    e => e.id === parseInt(req.params.id, 10)
  );
  eventsList[activityIndex].comments.push(comment);

  res.json({ comment });
});

server.get("/user/:userId/events", (req, res) => {
  const type = req.query.type;
  const offset = parseInt(req.query.offset || "0", 10);
  const mapper = id => eventsList.id === id;
  let result;
  if (type === "liked") {
    result = likedEventsList();
  } else if (type === "going") {
    result = goingEventsList();
  } else if (type === "past") {
    result = [];
  } else {
    res.sendStatus(404);
    return;
  }
  res.json({
    events: result.slice(offset, Math.min(offset + 20, result.length)),
    has_more: !(offset + 20 >= result.length)
  });
});

server.post("/event/:eventId/likes", (req, res) => {
  likedEvents[req.params.eventId] = true;
  // Find current event
  let activityIndex = eventsList.findIndex(
    e => e.id === parseInt(req.params.eventId, 10)
  );
  eventsList[activityIndex].likes.push({
    id: 128,
    username: "John Smith",
    avatar: "http://i.pravatar.cc/300?u=JohnSmith"
  });
  eventsList[activityIndex].is_liked = true;
  eventsList[activityIndex].likes_count += 1;
  res.json({
    event: eventsList[activityIndex]
  });
});
server.delete("/event/:eventId/likes/:userId", (req, res) => {
  likedEvents[req.params.eventId] = false;
  let activityIndex = eventsList.findIndex(
    e => e.id === parseInt(req.params.eventId, 10)
  );
  let likeIndex = eventsList[activityIndex].likes.findIndex(
    e => e.id === parseInt(req.params.userId, 10)
  );
  eventsList[activityIndex].likes.splice(likeIndex, 1);
  eventsList[activityIndex].is_liked = false;
  eventsList[activityIndex].likes_count -= 1;
  res.json({
    event: eventsList[activityIndex]
  });
});
server.post("/event/:eventId/participants", (req, res) => {
  goingEvents[req.params.eventId] = true;
  // Find current event
  let activityIndex = eventsList.findIndex(
    e => e.id === parseInt(req.params.eventId, 10)
  );
  eventsList[activityIndex].goings.push({
    id: 128,
    username: "John Smith",
    avatar: "http://i.pravatar.cc/300?u=JohnSmith"
  });
  eventsList[activityIndex].is_going = true;
  eventsList[activityIndex].goings_count += 1;
  res.json({
    event: eventsList[activityIndex]
  });
});
server.delete("/event/:eventId/participants/:userId", (req, res) => {
  goingEvents[req.params.eventId] = false;
  let activityIndex = eventsList.findIndex(
    e => e.id === parseInt(req.params.eventId, 10)
  );
  let goingIndex = eventsList[activityIndex].goings.findIndex(
    e => e.id === parseInt(req.params.userId, 10)
  );
  eventsList[activityIndex].goings.splice(goingIndex, 1);
  eventsList[activityIndex].is_going = false;
  eventsList[activityIndex].goings_count -= 1;
  res.json({
    event: eventsList[activityIndex]
  });
});

const rewriter = jsonServer.rewriter({});
server.use(rewriter);

server.use(router);

server.listen(3000, () => {
  console.log("JSON Server is running");
  console.log("Current unix time: " + moment().unix());
});

const eventTemplate = (rawIdx, channel, forceIdx = false, after, before) => {
  const idx = forceIdx ? rawIdx : channel.id * 100 + rawIdx;

  const startTime = after
    ? after + rawIdx * ((before - after) / MAX_EVENTS)
    : 1582605380 + 82000 + 3600 * idx;

  const isLiked = Math.random() >= 0.5;
  const isGoing = Math.random() >= 0.5;

  channel = eventToChannel[idx] || channel;
  eventToChannel[idx] = channel;

  const nGoings = getRandomInt(0, 20);
  let goings = [];
  for (var i = 0; i < nGoings; i++) {
    goings.push({
      id: getRandomInt(5000, 50000),
      username: "Commenter " + getRandomInt(5000, 50000),
      avatar: `http://i.pravatar.cc/300?u=${getRandomInt(5000, 50000)}`
    });
  }

  if (isGoing) {
    goings.push({
      id: 128,
      username: "John Smith",
      avatar: "http://i.pravatar.cc/300?u=JohnSmith"
    });
    goingEvents[idx] = true;
  }

  const nLikes = getRandomInt(0, 20);
  let likes = [];
  for (var i = 0; i < nLikes; i++) {
    likes.push({
      id: getRandomInt(5000, 50000),
      username: "Commenter " + getRandomInt(5000, 50000),
      avatar: `http://i.pravatar.cc/300?u=${getRandomInt(5000, 50000)}`
    });
  }

  if (isLiked) {
    likes.push({
      id: 128,
      username: "John Smith",
      avatar: "http://i.pravatar.cc/300?u=JohnSmith"
    });
    likedEvents[idx] = true;
  }

  let comments = [];
  const nComment = getRandomInt(10000, 100000);
  for (let i = 0; i < 20; i++) {
    comments.push({
      id: nComment + i,
      create_time: 1582605380 - i * 600,
      comment:
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium nemo culpa facere quaerat quas labore deserunt porro possimus doloremque nihil!",
      author: {
        id: getRandomInt(5000, 50000),
        username: "Commenter " + getRandomInt(5000, 50000),
        avatar: `http://i.pravatar.cc/300?u=${getRandomInt(5000, 50000)}`
      }
    });
  }

  return {
    id: idx,
    name:
      "Activity With ID " +
      idx +
      (Math.random() < 0.3
        ? " with really long title test for multi line display"
        : ""),
    begin_time: startTime,
    end_time: startTime + 7200,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio, laborum commodi nihil ad doloribus. Eos nesciunt aperiam fugit id odio, quos magni, a, nobis, dolor impedit animi. Ea nam quasi cum sit magni veniam est dolores qui ad soluta, molestias in ipsa blanditiis beatae officiis a perspiciatis iusto eveniet. Commodi.",
    creator: {
      id: 127,
      username: "John Smith",
      avatar: "http://i.pravatar.cc/300?u=JohnSmith"
    },
    create_time: 1582605380 - 7200 * idx,
    update_time: 1582605380 - 7200 * idx + 1000,
    channel: channel,
    goings: goings,
    likes: likes,
    images:
      Math.random() < 0.8
        ? []
        : [
            "https://via.placeholder.com/350x150",
            "https://via.placeholder.com/350x150",
            "https://via.placeholder.com/350x150"
          ],
    location: "Marina Bay Sands\n10 Bayfront Ave, S018956",
    goings_count: goings.length,
    likes_count: likes.length,
    is_going: isGoing,
    is_liked: isLiked,
    comments: comments
  };
};

setup();

const channels = channelChoices.reduce(
  (o, v) => ((o[v.id.toString()] = v), o),
  {}
);
