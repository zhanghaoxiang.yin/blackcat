import React, { Component } from 'react'
import { View, Button, Text } from 'react-native'

export default class Profile extends Component {
    constructor(props) {
        super(props)
        const {navigation,route}=this.props;
        const { current_id } = route.params;
        this.state = {
            name : '',
            email : '',
            id : current_id
          }
    }
    
    async componentDidMount() {
        const user_info = await fetch("http://localhost:3000/users" + `/${this.state.id}`, {
            method : 'GET'
        })
        try {
            const user = await user_info.json()
            this.setState({
                name : user.name,
                email : user.email
            })
            console.log(user)
            
        } catch (error) {
            alert(error)
            console.log(error)
        }
    }

    render() {
        
        return (
            <View>
                <Text> {this.state.id} </Text>
                <Text> {this.state.name} </Text>
                <Text> {this.state.email} </Text>
                <Button
                    title = 'Logout'
                    onPress = {() => this.props.navigation.popToTop()}
                />
                <Button
                    title = 'Create Event'
                    onPress = {() => this.props.navigation.navigate('CreateEvent', {
                        current_id : this.state.id
                    })}
                />
            </View>
        )
    }
}
