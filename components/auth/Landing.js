import React from 'react'
import { View, Button, TextInput, StyleSheet, ImageBackground, TouchableHighlight, Text } from 'react-native'

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    image: {
        flex: 1,
        resizeMode: 'stretch',
        opacity: 0.7,
        justifyContent : 'center',
        alignItems: 'center',
    },
    input: {
        height: 40,
        width: 240,
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 35,
        fontSize: 16
      },
    button :{
        textAlign:'center',
        justifyContent: 'center',
        width: '100%',
        height: 64,
        backgroundColor: 'yellow',
    },
    text: {
        alignItems:'center',
        justifyContent: 'center',
    }
  });

export default function Landing({ navigation }) {
    return (
        <View style = {styles.container}>
            <ImageBackground source={require('../../assets/Street-Dance-01.jpg')} style={styles.image}>
                <TouchableHighlight 
                    underlayColor="white"
                    style={styles.button}
                    onPress = {() => navigation.navigate('Login')}>
                    <View style={styles.text}>
                        <Text style={{fontSize:20}}>
                                Login
                        </Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight 
                    underlayColor="white"
                    style={styles.button}
                    onPress = {() => navigation.navigate('Register')}>
                    <View style={styles.text}>
                        <Text style={{fontSize:20}}>
                                Register
                        </Text>
                    </View>
                </TouchableHighlight>
            </ImageBackground>
        </View>
    )
}
