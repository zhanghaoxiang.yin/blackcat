import React, { Component } from 'react'
import { View, Text } from 'react-native'

export default class Loading extends Component {
    render() {
        return (
            <View style = {{ flex : 1, justifyContent : 'center'}}>
                <Text>Loading</Text>
            </View>
        )
    }
}
