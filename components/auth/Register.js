import React, { Component } from 'react'
import { View, Button, TextInput, StyleSheet, ImageBackground, TouchableHighlight, Text } from 'react-native'
import { withNavigation } from 'react-navigation';
import Profile from '../Profile'

const styles = StyleSheet.create({
    container: {
      flex: 1,

    },
    title: {
      marginTop: 16,
      paddingVertical: 8,
      borderWidth: 4,
      borderColor: "#20232a",
      borderRadius: 6,
      backgroundColor: "#61dafb",
      color: "#20232a",
      textAlign: "center",
      fontSize: 30,
      fontWeight: "bold"
    },
    image: {
        flex: 1,
        resizeMode: 'stretch',
        opacity: 0.7,
        justifyContent : 'flex-end',
        alignItems: 'center',
    },
    input: {
        height: 40,
        width: 240,
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 35,
        fontSize: 16
      },
    button :{
        textAlign:'center',
        justifyContent: 'center',
        width: '100%',
        height: 64,
        backgroundColor: 'yellow',
    },
    text: {
        alignItems:'center',
        justifyContent: 'center',
    }
  });

export class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            name : ''
        }

        this.onSignup = this.onSignup.bind(this);
    }

    async onSignup() {
        const user = {
            name : this.state.name,
            email : this.state.email,
            password : this.state.password
        }
        const user_id = await fetch("http://localhost:3000/users", {
            method : 'POST',
            body : JSON.stringify(user),
            headers : {'Content-Type' : 'application/json'}
        })
        try {
            const current_id = await user_id.json()
            console.log(current_id.id)
            this.props.navigation.replace('Profile', {
                current_id : current_id.id
            })
        } catch (error) {
            alert('Incorrect!')
            console.log(error)
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/Street-Dance-01.jpg')} style={styles.image}>
                    <TextInput
                        style={styles.input}
                        placeholder="Name"
                        placeholderTextColor="#000"
                        onChangeText={(name) => this.setState({name})}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="#000"
                        onChangeText={(email) => this.setState({email})}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        placeholderTextColor="#000"
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({password})}
                    />
                    <TouchableHighlight 
                        underlayColor="white"
                        style={styles.button}
                        onPress={() => this.onSignup()}>
                        <View style={styles.text}>
                            <Text style={{fontSize:20}}>
                                    SIGN UP
                            </Text>
                        </View>
                    </TouchableHighlight>
                </ImageBackground>
            </View>
        )
    }
}

export default withNavigation(Register)