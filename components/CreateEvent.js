import React, { Component } from 'react'
import { Button, View, TextInput } from 'react-native'

export default class CreateEvent extends Component {

    constructor(props) {
        super(props)
        const {navigation,route}=this.props;
        const { current_id } = route.params;
        this.state = {
            title : '',
            content : '',
            id : current_id
          }
          this.createEvent = this.createEvent.bind(this);
    }

    async createEvent() {
        const event = {
            title : this.state.title,
            content : this.state.content,
            id: this.state.id
        }
        console.log(JSON.stringify(event))
        const new_event = await fetch("http://localhost:3000/event", {
            method : 'POST',
            body : JSON.stringify(event),
            headers : {'Content-Type' : 'application/json'}
        })
        try {
            const created_event = await new_event.json()
            console.log(created_event)
            
        } catch (error) {
            alert('Error!')
            console.log(error)
        }
    }

    render() {
        return (
            <View>
                <TextInput
                    placeholder="Title"
                    placeholderTextColor="#000"
                    onChangeText={(title) => this.setState({title})}
                />
                <TextInput
                    placeholder="Content"
                    placeholderTextColor="#000"
                    onChangeText={(content) => this.setState({content})}
                />
                <Button
                    title='Create'
                    onPress={() => this.createEvent()}
                />
            </View>
        )
    }
}
