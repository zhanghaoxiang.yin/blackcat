if (__DEV__) {
  import("./ReactotronConfig").then(() => {
    console.log("Reactotron Configured");
  });
}
import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import LandingScreen from './components/auth/Landing'
import RegisterScreen from './components/auth/Register'
import LoginScreen from './components/auth/Login'
import ProfileScreen from './components/Profile'
import CourtScreen from './components/Court'
import CreateEventScreen from './components/CreateEvent'
import { View, Text } from 'react-native'

const Stack = createStackNavigator();

export class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loaded : false,
    }
  }

  // componentDidMount() {
  //   firebase.auth().onAuthStateChanged((user) => {
  //     if (!user) {
  //       this.setState({
  //         loggedIn : false,
  //         loaded : true
  //       })
  //     } else {
  //       this.setState({
  //         loggedIn : true,
  //         loaded : true
  //       })
  //     }
  //   })
  // }

  render() {
    console.tron.log("hello world");
    const {loggedIn, loaded} = this.state;
    // if (!loaded) {
    //   return <LoadingScreen/>
    // }
      return (
        <NavigationContainer>
          <Stack.Navigator initialRouteName = 'Landing'>
            <Stack.Screen name = 'Landing' component = {LandingScreen} options={{headerShown: false}}/>
            <Stack.Screen name = 'Register' component = {RegisterScreen} options={{headerShown: false}}/>
            <Stack.Screen name = 'Login' component = {LoginScreen} options={{headerShown: false}}/>
            <Stack.Screen name = 'Profile' component = {ProfileScreen} options = {{headerShown: false, headerLeft: null, gesturesEnabled: false}}/>
            <Stack.Screen name = 'Court' component = {CourtScreen} options = {{headerShown: false, headerLeft: null, gesturesEnabled: false}}/>
            <Stack.Screen name = 'CreateEvent' component = {CreateEventScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      )
  }
}

export default App

